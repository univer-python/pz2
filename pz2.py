from tkinter import *
  
root = Tk()
root.title("Резюме")

lb = Label(text="Резюме", font=("Arial", 32))
lb.pack()

def input_text(label):
    lb_frame = LabelFrame(root, text = label)
    lb_frame.pack()

    entry = Entry(lb_frame)
    entry.pack()


input_text("Прізвище:")
input_text("Ім'я:")
input_text("По батькові:")
input_text("Посада 1:")
input_text("Посада 2:")
input_text("Посада 3:")

# ----------- CheckButtons --------
lb_frame = LabelFrame(root, text = "Мови програмування,  \nякими володієте:")
lb_frame.pack()

py = BooleanVar()

Checkbutton(lb_frame, text = "Python", variable=py).grid(row=0, sticky=W)
Checkbutton(lb_frame, text = "C/C++").grid(row=1, sticky=W)
Checkbutton(lb_frame, text = "Java").grid(row=2, sticky=W)
Checkbutton(lb_frame, text = "C#").grid(row=3, sticky=W)
Checkbutton(lb_frame, text = "Swift").grid(row=4, sticky=W)
Checkbutton(lb_frame, text = "Kotlin").grid(row=5, sticky=W)
Checkbutton(lb_frame, text = "Dart").grid(row=6, sticky=W)
# ---------- END checkbuttons -----


# ---------- RadioButtons -------------
lb_frame = LabelFrame(root, text = "Стать:")
lb_frame.pack()

sex = IntVar()
k = 0

def create_radio(label):
    global k
    Radiobutton(lb_frame, text=label, value=k+1, variable=sex).grid(row=k, sticky=W)
    k += 1

create_radio("Чоловіча")
create_radio("Жіноча")
create_radio("Інша")
# ---------- END radiobuttons ---------


# ---------- ListBox ------------------
lb_frame = LabelFrame(root, text = "Альма-матер:")
lb_frame.pack()

arr = ["ХНУ", "ХНУРЕ", "ХНЕУ"]
box = Listbox(lb_frame, height=len(arr) + 1, width=10) 
for i in arr:
    box.insert(END, i)

box.pack()
# ---------- END listbox --------------


lb_frame = LabelFrame(root, text = "Коментарі/додаток:")
lb_frame.pack()

Text(lb_frame, width=20, height=8).pack()

# ---------- Button -------------------
but = Button(root,
             text="Отправить",
             height=1,
             bg="#00ffcc", fg="red",
             font=("Arial", 24)
             )

def btnClick(event):
    print("Send button was clicked")

    
but.bind("<Button-1>", btnClick)

but.pack()
# ---------- END button ---------------

root.mainloop()

